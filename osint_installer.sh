#! /bin/bash
# osint_install.sh - Automatically prepare a clean Osint enviroment

# INSTALL NORDVPN
echo "Installing NordVPN..."
cd ~/Downloads
wget "https://repo.nordvpn.com/deb/nordvpn/debian/pool/main/nordvpn-release_1.0.0_all.deb"
sudo dpkg -i nordvpn-release*
sudo apt update
sudo apt install -y nordvpn
# nordvpn login
# nordvpn set killswitch enabled
# nordvpn set cybersec enabled
# nordvpn set obfuscate enabled
# nordvpn set autoconnect enabled
# nordvpn c

# INSTALL OSINT TOOLS-BASIC
echo "Installing basic OSINT tools..."
sudo apt purge -y apport
sudo apt remove -y popularity-contest
sudo snap install vlc
sudo apt update
sudo apt install -y ffmpeg
sudo apt install -y python3-pip
sudo python3 -m pip install youtube_dl
sudo python3 -m pip install youtube-tool
sudo python3 -m pip install xeuledoc
cd ~/Desktop
sudo apt install -y curl
curl -u osint8:book4529zw -O https://inteltechniques.com/osintbook8/vm-files.zip
unzip vm-files.zip -d ~/Desktop/
mkdir ~/Documents/scripts
mkdir ~/Documents/icons
cd ~/Desktop/vm-files/scripts
cp * ~/Documents/scripts
cd ~/Desktop/vm-files/icons
cp * ~/Documents/icons
cd ~/Desktop/vm-files/shortcuts
sudo cp * /usr/share/applications/
cd ~/Desktop
rm vm-files.zip
rm -rf vm-files
sudo python3 -m pip install --upgrade streamlink
sudo python3 -m pip install Instalooter
sudo python3 -m pip install Instaloader
sudo snap install gallery-dl
sudo apt install -y git
mkdir ~/Downloads/Programs
cd ~/Downloads/Programs
git clone https://github.com/sherlock-project/sherlock.git
cd sherlock
sudo python3 -m pip install -r requirements.txt
sudo python3 -m pip install socialscan
sudo python3 -m pip install holehe
cd ~/Downloads/Programs
git clone https://github.com/WebBreacher/WhatsMyName.git
cd WhatsMyName
sudo python3 -m pip install -r requirements.txt
cd ~/Downloads/Programs
git clone https://github.com/ChrisTruncer/EyeWitness.git
cd EyeWitness/Python/setup
sudo -H ./setup.sh
sudo snap install amass
cd ~/Downloads/Programs
git clone https://github.com/aboul3la/Sublist3r.git
cd Sublist3r
sudo python3 -m pip install -r requirements.txt
cd ~/Downloads/Programs
git clone https://github.com/s0md3v/Photon.git
cd Photon && python3 -m pip install -r requirements.txt
cd ~/Downloads/Programs
git clone https://github.com/laramies/theHarvester.git
cd theHarvester
sudo python3 -m pip install -r requirements.txt --ignore-installed
sudo python3 -m pip install pipenv
sudo python3 -m pip install webscreenshot
sudo add-apt-repository -y ppa:micahflee/ppa
sudo apt -y update
sudo apt install -y torbrowser-launcher
cd ~/Downloads
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install -y ./google-chrome-stable_current_amd64.deb
sudo rm google-chrome-stable_current_amd64.deb
sudo apt install -y mediainfo-gui
sudo apt install -y libimage-exiftool-perl
sudo apt install -y mat2
sudo apt install -y webhttrack
wget http://dl.google.com/dl/earth/client/current/google-earth-stable_current_amd64.deb
sudo apt install -y ./google-earth-stable_current_amd64.deb
sudo rm google-earth-stable_current_amd64.deb
sudo apt install -y kazam
sudo snap install keepassxc
sudo apt  snap-y cherrytree
sudo apt install -y qbittorrent
sudo apt install -y ufw
sudo apt install -y gufw
sudo ufw enable

# INSTALL FIREFOX PROFILE
echo "Installing firefox profile..."
echo "Close firefox browser!"
firefox & sleep 15 && sudo pkill firefox
cd ~/Desktop
curl -u osint8:book4529zw -O https://inteltechniques.com/osintbook8/ff-template.zip
unzip ff-template.zip -d ~/.mozilla/firefox/
cd ~/.mozilla/firefox/ff-template/
cp -R * ~/.mozilla/firefox/*.default-release
cd ~/Desktop
rm ff-template.zip
echo "Done."

# INSTALL SEARCH TOOLS
cd ~/Desktop
curl -u osint8:book4529zw -O https://inteltechniques.com/osintbook8/tools.zip
unzip tools.zip -d ~/Desktop/
rm tools.zip
echo "Done."

# INSTALL OSINT TOOLS-ADVANCED
echo "Installing OSINT tools advanced..."
cd ~/Downloads/Programs
git clone https://github.com/opsdisk/metagoofil.git
cd metagoofil
sudo python3 -m pip install -r requirements.txt
cd ~/Downloads/Programs
git clone https://github.com/lanmaster53/recon-ng.git
cd recon-ng
sudo python3 -m pip install -r REQUIREMENTS
cd ~/Downloads/Programs
git clone https://github.com/smicallef/spiderfoot.git
cd spiderfoot
sudo python3 -m pip install -r requirements.txt
cd ~/Downloads/Programs
git clone https://github.com/AmIJesse/Elasticsearch-Crawler.git
sudo python3 -m pip install nested-lookup
sudo python3 -m pip install internetarchive
sudo apt install -y ripgrep
sudo python3 -m pip install bdfr --upgrade
sudo python3 -m pip install redditsfinder
sudo python3 -m pip install waybackpy
sudo python3 -m pip install testresources
echo "Done."

# USER INTERFACE CONFIGURATION
echo "Fixing user interface..."
gsettings set org.gnome.desktop.background picture-uri ''
gsettings set org.gnome.desktop.background primary-color 'rgb(66, 81, 100)'
gsettings set org.gnome.shell favorite-apps []
gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
gsettings set org.gnome.shell favorite-apps "['firefox.desktop', 'google-chrome.desktop', 'torbrowser.desktop', 'org.gnome.Nautilus.desktop', 'org.gnome.Terminal.desktop', 'updates.desktop', 'tools.desktop', 'youtube_dl.desktop', 'ffmpeg.desktop', 'streamlink.desktop', 'instagram.desktop', 'gallery.desktop', 'usertool.desktop', 'eyewitness.desktop', 'domains.desktop', 'metadata.desktop', 'httrack.desktop', 'metagoofil.desktop', 'elasticsearch.desktop', 'reddit.desktop', 'internetarchive.desktop', 'spiderfoot.desktop', 'recon-ng.desktop', 'mediainfo-gui.desktop', 'google-earth-pro.desktop', 'kazam.desktop', 'keepassxc_keepassxc.desktop', 'gnome-control-center.desktop']"
gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 32
echo "Done."

# AUTOREMOVE PROGRAMS
sudo apt autoremove -y

# LAST STEP MESSAGE
echo "**Last steps**
* Setup NordVPN
* Disable screen lock
* Disable file history
* Disable automatic suspend
* Disable telemetry" > ~/Desktop/cleanup.txt
